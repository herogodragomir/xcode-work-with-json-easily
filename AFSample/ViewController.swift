//
//  ViewController.swift
//  AFSample
//
//  Created by Edy Cu Tjong on 6/10/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Alamofire.request("https://codewithchris.com/code/afsample.json").responseJSON { (response) in
            // Check if the result has a value
            if let value = response.result.value {
                let json = JSON(value)
                print(json["firstkey"].stringValue)
                print(json["secondkey"][1].stringValue)
            }
        }
    }


}

